import { FaFacebook, FaInstagram, FaGithub } from "react-icons/fa";
import logo from "../images/logo.svg";

const Footer = () => {
  return (
    <footer>
      <a href="/" className="brand">
        <img src={logo} alt="Logo" className="brand-img" />
        Cooking Oil
      </a>
      <ul className="scm-list">
        <li>
          <a
            className="scm-icon"
            href="https://facebook.com/adi.permana.10004"
            rel="noopener noreferrer"
            target="_blank"
          >
            <FaFacebook />
          </a>
        </li>
        <li>
          <a
            className="scm-icon"
            href="https://instagram.com/permanaaa_"
            rel="noopener noreferrer"
            target="_blank"
          >
            <FaInstagram />
          </a>
        </li>
        <li>
          <a
            className="scm-icon"
            href="https://gitlab.com/adipermana.xpbr3"
            rel="noopener noreferrer"
            target="_blank"
          >
            <FaGithub />
          </a>
        </li>
      </ul>
      <p className="copyright-text">
        <small>&copy; Copyright 2021 Cooking Oil</small>
      </p>
    </footer>
  );
};

export default Footer;
